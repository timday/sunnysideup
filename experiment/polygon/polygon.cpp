#include <iostream>
#include <boost/polygon/polygon.hpp>
#include <vector>

// Some typedefs
namespace bpl = boost::polygon;
typedef bpl::polygon_data<double> Polygon;
typedef bpl::polygon_traits<Polygon>::point_type Point;

int main() {

  // Your C-style data (assumed (x,y) pairs)
  double * data = (double *) malloc(sizeof(double) * 100 * 2);
  for (int ii = 0; ii < 100; ii++) {
    data[2*ii] = ii;
    data[2*ii + 1] = ii;
  }

  // Convert to points
  std::vector<Point> points;
  for (int i=0;i<100;++i)
    points.push_back(Point(data[2*i],data[2*i+1]));
  
  // Create a polygon
  Polygon polygon;
  polygon.set(points.begin(),points.end());

  // Do something with the polygon
  std::cout << "Perimeter : " << bpl::perimeter(polygon) << std::endl;
  std::cout << "Area      : " << bpl::area(polygon) << std::endl;
  
  return 0;
}
