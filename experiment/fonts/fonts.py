#!/usr/bin/env python

# Based on http://cairographics.org/pycairo_pango/
# Lists all fonts known to Pango and dumps an image of them all to cairo_fonts.png

import cairo
import Image
import numpy as np
import pango
import pangocairo
import sys

def render(fontname):

    print fontname
    
    surf = cairo.ImageSurface(cairo.FORMAT_A8, 1024, 75)
    context = cairo.Context(surf)

    # Translates context so that desired text upperleft corner is at 0,0
    context.translate(25,25)
    #context.set_antialias(cairo.ANTIALIAS_SUBPIXEL)

    pangocairo_context = pangocairo.CairoContext(context)
    pangocairo_context.set_antialias(cairo.ANTIALIAS_SUBPIXEL)
    
    layout = pangocairo_context.create_layout()

    font = pango.FontDescription(fontname + " 16")
    layout.set_font_description(font)
    
    layout.set_text(unicode(fontname)+u': "Problems are inevitable.  Problems are soluble."')
    context.set_source_rgb(0, 0, 0)
    pangocairo_context.update_layout(layout)
    pangocairo_context.show_layout(layout)

    img=np.reshape(
        np.frombuffer(surf.get_data(),dtype=np.uint8),
        (surf.get_height(),surf.get_width())
        )

    img[0,:]=128
    return img

def main():
    
    #get font families:
    font_map = pangocairo.cairo_font_map_get_default()
    families = font_map.list_families()
    
    fontnames=sorted((f.get_name() for f in font_map.list_families()))

    imgs=map(render,fontnames)
    img=np.vstack(imgs)

    Image.fromarray(255-img).save("cairo_fonts.png")
    
if __name__ == "__main__":
    main()
