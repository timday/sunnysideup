#!/usr/bin/env python

# total_size code from http://code.activestate.com/recipes/577504/

from sys import getsizeof, stderr
from itertools import chain
from collections import deque

def total_size(o, handlers={}):
    """ Returns the approximate memory footprint an object and all of its contents.

    Automatically finds the contents of the following builtin containers and
    their subclasses:  tuple, list, deque, dict, set and frozenset.
    To search other containers, add handlers to iterate over their contents:

        handlers = {SomeContainerClass: iter,
                    OtherContainerClass: OtherContainerClass.get_elements}

    """
    dict_handler = lambda d: chain.from_iterable(d.items())
    all_handlers = {tuple: iter,
                    list: iter,
                    deque: iter,
                    dict: dict_handler,
                    set: iter,
                    frozenset: iter,
                   }
    all_handlers.update(handlers)     # user handlers take precedence
    seen = set()                      # track which object id's have already been seen
    default_size = getsizeof(0)       # estimate sizeof object without __sizeof__

    def sizeof(o):
        if id(o) in seen:       # do not double count the same object
            return 0
        seen.add(id(o))
        s = getsizeof(o, default_size)

        for typ, handler in all_handlers.items():
            if isinstance(o, typ):
                s += sum(map(sizeof, handler(o)))
                break
        return s

    return sizeof(o)


N=1000000

a0=[x for x in range(N)]
a1=[x+1 for x in range(N)]
a2=[x+2 for x in range(N)]

b=[(x,x+1,x+2) for x in range(N)]

print total_size(a0)
print total_size(a1)
print total_size(a2)
print total_size(b)

print float(total_size(b))/float(total_size(a0)+total_size(a1)+total_size(a2))
