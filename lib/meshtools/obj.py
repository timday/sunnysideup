import mesh

def dump_obj(mesh,filename):
    vertices,faces=mesh.vertices(),mesh.faces()
    with open(filename,'w') as f:
        f.write('# File created by "sunnysideup" project\n')
        f.write('# {0} vertices\n'.format(len(vertices)))
        f.write('# {0} faces\n'.format(len(faces)))
        f.write('\n')
        for v in vertices:
            f.write('v {0} {1} {2}\n'.format(v[0],-v[2],v[1]))
        f.write('\n')
        for q in faces:
            f.write('f')
            for v in q:
                f.write(' {0}'.format(v+1))  # OBJ uses 1-based vertex numbering
            f.write('\n')

def load_obj(filename):
    vertices=[]
    faces=[]
    with open(filename,'r') as f:
        for txt in f:
            items=txt.strip().split()
            if len(items)>0:
                if items[0]=='v':
                    coords=map(float,items[1:])
                    assert len(coords)==3
                    vertices.append((coords[0],coords[2],-coords[1]))
                elif items[0]=='f':
                    indices=map(lambda i: int(i)-1,items[1:])  # Correct for OBJ's 1-based vertex numbering
                    assert len(indices)==3 or len(indices)==4
                    faces.append(indices)

    return mesh.Mesh(vertices,faces)
