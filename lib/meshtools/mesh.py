from itertools import *

def _dot(a,b):
    return a[0]*b[0]+a[1]*b[1]+a[2]*b[2]

class Mesh(object):

    def __init__(self,v=None,f=None):
        self._vertices=v
        self._faces=f

    def vertices(self):
        return self._vertices

    def num_vertices(self):
        return len(self._vertices)

    def faces(self):
        return self._faces

    def num_faces(self):
        return len(self._faces)

    # Return the i-th face as 1 or 2 triangles depending on whether it's a triangle or a quad
    def _split_face(self,i):
        f=self._faces[i]
        assert len(f)==3 or len(f)==4
        if len(f)==3:
            return (f,)
        else:
            if i%2==0:
                return ((f[0],f[1],f[2]),(f[2],f[3],f[0]))
            else:
                return ((f[0],f[1],f[3]),(f[3],f[1],f[2]))

    def triangles(self):
        return chain.from_iterable(
            (self._split_face(i) for i in xrange(self.num_faces()))
            )

    # Return a copy of the mesh with faces with any vertex p p.d>t clipped
    # Deosn't remove any n-longer needed vertices
    def clipped(self,direction,threshold):

        def retained(face):
            for i in face:
                if _dot(self._vertices[i],direction)>threshold:
                    return False
            return True

        newfaces=filter(retained,self._faces)
        return Mesh(self._vertices,newfaces)

    def upsidedown(self):
        newvertices=[(-v[0],-v[1],v[2]) for v in self._vertices]
        return Mesh(newvertices,self._faces)
