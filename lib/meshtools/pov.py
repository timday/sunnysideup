from operator import *

import mesh

def dump_pov(mesh,filename_inc,filename_pov,ground_plane,parameters):
    vertices=mesh.vertices()
    triangles=list(mesh.triangles())
    
    miny=min(map(itemgetter(1),vertices))

    with open(filename_inc,'w') as f:
        f.write('// File created by "sunnysideup" project\n')
        f.write('// {0} vertices\n'.format(len(vertices)))
        f.write('// {0} triangles\n'.format(len(triangles)))
        f.write('\n')

        f.write('mesh2 {\n')
        f.write('  vertex_vectors {\n')
        f.write('    {0},\n'.format(len(vertices)))
        f.write(',\n'.join(('    <{0},{1},{2}>'.format(v[0],v[1],-v[2]) for v in vertices)))
        f.write('\n  }\n')
        f.write('  face_indices {\n')
        f.write('    {0},\n'.format(len(triangles)))
        f.write(',\n'.join(('    <{0},{1},{2}>'.format(t[0],t[1],t[2]) for t in triangles)))
        f.write('\n  }\n')
        f.write('  texture { pigment {rgb <1,1,1>} finish {ambient 0 diffuse 0.9 specular 0.1} }\n')
        f.write('}\n')

    with open(filename_pov,'w') as f:

        camera=parameters.get('camera',(-110.0,0.0,110.0)) # NB to agree with yafaray, then flipped below
        light=parameters.get('light',(-500.0,-500.0,500.0))

        f.write('camera {{perspective location <{0},{1},{2}> look_at <0,0,0> angle 22.5}}\n'.format(camera[0],camera[2],camera[1]))
        f.write('light_source {{<{0},{1},{2}> color rgb<1,1,1>}}\n'.format(light[0],light[2],light[1]))
        if ground_plane:
            f.write('plane {\n')
            f.write(' <0.0,1.0,0.0>,{0}\n'.format(miny))
            f.write(' texture { pigment {rgb <0.5,0.5,0.5>} finish {ambient 0 diffuse 1} }\n')
            f.write('}\n')
        f.write('#include "{0}"\n'.format(filename_inc))
