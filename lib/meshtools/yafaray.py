from operator import *
import xml.etree.cElementTree as ET

import mesh

# Remove leading and trailing whitespace from literal fragments with pretty formatting.
def _cannonical(s):
    return reduce(
        lambda x,y: x+y,
        map(
            lambda x: x.strip(),
            s.split('\n')
            )
        )

def _make_yafaray_meshxml(mesh):
    print "Creating mesh XML..."
    vertices=mesh.vertices()
    triangles=mesh.triangles()

    meshxml=ET.Element('mesh',{'has_uv':'false','type':'0'})
    for v in vertices:
        meshxml.append(
            ET.Element('p',{'x':str(v[0]),'y':str(-v[2]),'z':str(v[1])})
            )
    meshxml.append(
        ET.Element('set_material',{'sval':'material.bezel'})
        )
    for t in triangles:
        meshxml.append(
            ET.Element('f',{'a':str(t[0]),'b':str(t[1]),'c':str(t[2])})
            )
    print "...created mesh XML"
    return meshxml

# Dump multiple yafaray outputs.
# filenames_xml is (filename,quality) tuples.  quality 0 is ray-cast, -1 is clay, 1 is path tracing
def dump_yafaray(mesh,filenames_xml,ground_plane,parameters):
        
    miny=min(map(itemgetter(1),mesh.vertices()))

    meshxml=_make_yafaray_meshxml(mesh)

    canvas_width=parameters.get('canvas_width',1024)
    canvas_height=parameters.get('canvas_height',768)

    for (filename_xml,quality) in filenames_xml:

        scene=ET.Element('scene',{'type':'triangle'})
        
        if quality==-1:
            scene.append(
                ET.fromstring(
                    _cannonical(
                        '''
    <material name="material.bezel">
      <type sval="shinydiffusemat"/>
      <color r="1" g="1" b="1" a="1"/>
      <diffuse_reflect fval="1"/>
    </material>
    '''
                        )
                    )
                )
        else:
            scene.append(
                ET.fromstring(
                    _cannonical(
                        '''
    <material name="material.bezel">
      <type sval="shinydiffusemat"/>
      <color r="1" g="1" b="1"/>
      <mirror_color r="1" g="1" b="1"/>
      <diffuse_reflect fval="0.9"/>
      <specular_reflect fval="0.1"/>
    </material>
    '''
                        )
                    )
                )
    
        if quality==-1:
            scene.append(
                ET.fromstring(
                    _cannonical(
                        '''
    <material name="material.floor">
      <type sval="shinydiffusemat"/>
      <color r="1" g="1" b="1" a="1"/>
      <diffuse_reflect fval="1"/>
    </material>
    '''
                        )
                    )
                )
        else:
            scene.append(
                ET.fromstring(
                    _cannonical(
                        '''
    <material name="material.floor">
      <type sval="shinydiffusemat"/>
      <color r="0.5" g="0.5" b="0.5"/>
      <mirror_color r="1" g="1" b="1"/>
      <diffuse_reflect fval="1.0"/>
      <specular_reflect fval="0.0"/>
    </material>
    '''
                        )
                    )
                )
    
        scene.append(meshxml)
        
        scene.append(
            ET.fromstring(
                _cannonical(
                    '''
    <smooth ID="0" angle="60"/> 
    '''
                    )
                )
            )
    
        if ground_plane:
            scene.append(
                ET.fromstring(
                    _cannonical(
                    '''
    <mesh has_uv="false" type="0">
      <p x="-1000.0" y="-1000.0" z="{0}"/>
      <p x=" 1000.0" y="-1000.0" z="{0}"/>
      <p x=" 1000.0" y=" 1000.0" z="{0}"/>
      <p x="-1000.0" y=" 1000.0" z="{0}"/>
      <set_material sval="material.floor"/>
      <f a="0" b="1" c="2"/>
      <f a="2" b="3" c="0"/>
    </mesh>
    '''.format(miny)
                        )
                    )
                )
    
        # Clay render uses no lamps
        if quality!=-1:
            light=parameters.get('light',(-500.0,-500.0,500.0))
            scene.append(
                ET.fromstring(
                    _cannonical(
                        '''
    <light name="lamp">
      <type sval="directional"/>
      <color r="1" g="1" b="1"/>
      <direction x="{0}" y="{1}" z="{2}"/>
      <infinite bval="true"/>
      <power fval="1.0"/>
    </light>
    '''.format(light[0],light[1],light[2])
                        )
                    )
                )
            
        scene.append(
            ET.fromstring(
                _cannonical(
                    '''
    <background name="background">
      <type sval="sunsky" />
      <from x="-500.0" y="-500.0" z="500.0"/>
      <turbidity fval="3.0" />
      <a_var fval="0.7" />
      <b_var fval="0.2" />
      <c_var fval="0.5" />
      <d_var fval="1.0" />
      <e_var fval="1.0" />
      <power fval="1.0" />
    </background>
    '''
                    )
                )
            )
        
        camera=parameters.get('camera',(-110.0,0.0,110.0))
        camera_up=parameters.get('camera_up',(1.0,0.0,1.0))
        scene.append(
            ET.fromstring(
                _cannonical(
                    '''
    <camera name="camera">
      <type sval="perspective"/>
      <from x="{0}" y="{1}" z="{2}"/>
      <to x="0" y="0" z="0"/>
      <up x="{3}" y="{4}" z="{5}"/>
      <resx ival="{6}"/>
      <resy ival="{7}"/>
      <aperture fval="0"/>
      <focal fval="3.0"/>
      <bokeh_rotation fval="0"/>
      <bokeh_type sval="disk1"/>
    </camera>
    '''.format(camera[0],camera[1],camera[2],camera_up[0],camera_up[1],camera_up[2],canvas_width,canvas_height)
                    )
                )
            )
    
        if quality==1:
            scene.append(
                ET.fromstring(
                    _cannonical(
                        '''
    <integrator name="integrator">
      <type sval="pathtracing"/>
      <use_background bval="true"/>
    </integrator>
    '''
                        )
                    )
                )
        elif quality==-1:
            scene.append(
                ET.fromstring(
                    _cannonical(
                        '''
    <integrator name="integrator">
      <type sval="directlighting"/>
      <raydepth ival="5"/>
      <transpShad bval="true"/>
      <shadowDepth ival="5"/>
      <do_AO bval="true"/>
      <AO_samples ival="64"/>
      <AO_distance fval="10.0"/>
      <AO_color r="1" g="1" b="1" a="1"/>
    </integrator>
    '''
                        )
                    )
                )
        else:
            scene.append(
                ET.fromstring(
                    _cannonical(
                        '''
    <integrator name="integrator">
      <type sval="directlighting"/>
      <raydepth ival="5"/>
      <transpShad bval="true"/>
      <shadowDepth ival="5"/>
    </integrator>
    '''
                        )
                    )
                )
        
        scene.append(
            ET.fromstring(
                _cannonical(
                    '''
    <integrator name="volintegrator">
      <type sval="none"/>
    </integrator>
    '''
                    )
                )
            )
    
        scene.append(
            ET.fromstring(
                _cannonical(
                    '''
    <render>
      <AA_minsamples ival="4"/>
      <AA_inc_samples ival="4"/>
      <AA_passes ival="2"/>
      <AA_pixelwidth fval="1.5"/>
      <AA_threshold fval="0.01"/>
      <background_name sval="background"/>
      <camera_name sval="camera"/>
      <clamp_rgb bval="true"/>
      <filter_type sval="mitchell"/>
      <gamma fval="2.2"/>
      <width ival="{0}"/>
      <height ival="{1}"/>
      <integrator_name sval="integrator"/>
      <volintegrator_name sval="volintegrator"/>
      <threads ival="-1"/>
      <xstart ival="0"/>
      <ystart ival="0"/>
      <z_channel bval="false"/>
      </render>
    '''.format(canvas_width,canvas_height)
                    )
                )
            )
        
        tree=ET.ElementTree(scene)

        print 'Writing {0} (quality {1})...'.format(filename_xml,quality)
        with open(filename_xml,'w') as f:
            # tree.write(f,'us-ascii',True) # Python2.7 version to get XML header.
            tree.write(f)      # xml.etree has no prettyprinting option, surprisingly
        print '...wrote {0}'.format(filename_xml)
