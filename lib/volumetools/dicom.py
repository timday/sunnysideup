# -*- coding: utf-8 -*-

import ConvertNumpy
import gdcm
from multiprocessing import Pool
import numpy as np
import operator
import os
import sys

def _dot():
    sys.stdout.write('.')
    sys.stdout.flush()

def loadDicomFile(filename):
    reader=gdcm.ImageReader()
    reader.SetFileName(filename)
    if reader.Read():
        src=reader.GetImage()
        spacing=gdcm.ImageHelper.GetSpacingValue(reader.GetFile()) 
        origin=gdcm.ImageHelper.GetOriginValue(reader.GetFile())
        dimensionless_z=origin[2]/spacing[0]
        dst=ConvertNumpy.gdcm_to_numpy(src)
        assert dst.shape[0]==src.GetDimension(1)
        assert dst.shape[1]==src.GetDimension(0)
        _dot()
        return dst,dimensionless_z
    else:
        raise Exception("GDCM failed to read {0}".format(filaname))

def loadDicomDir(path,suffix):
    print path
    files=[path+'/'+f for f in sorted(os.listdir(path)) if len(f)>len(suffix) and f[-len(suffix):]==suffix]
    pool=Pool()
    loaded=pool.map(loadDicomFile,files)
    slices,z=zip(*loaded)
    zlo=min(z[0],z[-1])
    zhi=max(z[0],z[-1])
    dimensionless_z_spacing=(zhi-zlo)/(len(z)-1)
    print
    print 'Dimensionless z-spacing: {0}'.format(dimensionless_z_spacing)

    n=len(slices)
    vol=np.zeros([n,512,512],dtype=np.int16)
    for i in xrange(n):
        vol[i,:,:]=slices[i]
    print ""
    return vol,(1.0,1.0,dimensionless_z_spacing)

def load_volume_phenix():
    return loadDicomDir(
        os.path.expanduser('~/download/volume_data/dicom/osirix/PHENIX/CT2 tête, face, sinus/COU IV'),
        '.dcm'
        )
    
def load_volume_manix():
    return loadDicomDir(
        os.path.expanduser('~/download/volume_data/dicom/osirix/MANIX/CER-CT/ANGIO CT'),
        '.dcm'
        )
    
