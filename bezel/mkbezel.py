#!/usr/bin/env python

import cairo
import Image
from itertools import *
import math
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import numpy as np
import numpy.random
from optparse import OptionParser
import os
import pango
import pangocairo
import random
import sys

sys.path.append(os.path.abspath('../lib'))
import meshtools.mesh
import meshtools.obj
import meshtools.pov
import meshtools.yafaray

def sqr(x):
    return x*x

def mktextimage(txts,width,height):
    surface=cairo.ImageSurface(cairo.FORMAT_A8,width,height)
    ctx = cairo.Context(surface)

    # 'sans-serif' works.
    # 'Courier', 'Courier New', 'helvetica' also promising
    # Supposedly want pycairo pango for more text options.
    # Aha: use ../experiment/fonts/fonts.py to list & render available fonts on system

    # Good choices: 'Sans', 'Droid Sans', 'Inconsolata'
    # cairo.FONT_WEIGHT_BOLD a good idea
    # 'Sans' in particular satisfyingly wide
    # 'Droid Sans Mono' too squished 'm's
    ctx.select_font_face('Sans',cairo.FONT_SLANT_NORMAL,cairo.FONT_WEIGHT_BOLD)  

    font_size=1.0
    while True:
        ctx.set_font_size(font_size)    
        xpadding=ctx.text_extents('  ')[4]
        extents=map(ctx.text_extents,txts)
        if max((xpadding+e[2] for e in extents))>width/len(txts) or max((e[3] for e in extents))>(2*height)/3:
            font_size=font_size-1
            vertical_offset=height-(height-max((e[3] for e in extents)))/2
            break
        font_size=font_size+1.0
    
    ctx.set_font_size(font_size)    
    
    for i in xrange(len(txts)):
        
        xpadding=(ctx.text_extents('  ')[4]+width/len(txts)-ctx.text_extents(txts[i])[4])/2.0

        ctx.new_path()
        ctx.move_to((i*width)/len(txts)+xpadding,vertical_offset)
        ctx.show_text(txts[i])

    img=np.reshape(
        np.frombuffer(surface.get_data(),dtype=np.uint8),
        (surface.get_height(),surface.get_width())
        )

    return 1.0-np.float32(img)/255.0

# Given an image, find the interesting y value in column x
def findy(src,x):
    for y in xrange(1,src.shape[0]):
        if src[y,x]!=src[y-1,x]:
            return y
    raise Exception("Can't find transition in column {0}".format(x))

# Make an image of the decor as a scale-factor to be applied to the bezel "display area"
def mkdecorimage(what,width,height,filename):
    if what=='flat':
        img=np.zeros((height,width))
    elif what=='random':
        img=np.random.random((height,width))
    elif what=='dots':
        img=np.ones((height,width))
        Y=height/2
        R=height/3
        for X in [(i*width)/12 for i in xrange(0,12)]:
            for y in xrange(-R,R+1):
                for x in xrange(-R,R+1):
                    r=math.sqrt(x*x+y*y)
                    if r<R:
                        img[Y+y,(X+x+width)%width]=sqr(r/R)
    elif what=='cuillin':
        src=np.asarray(Image.open('data/cuillin-profile.png'))
        if len(src.shape)>2:
            src=src[:,:,0]
        ys=np.array([findy(src,x) for x in xrange(src.shape[1])])
        ys=ys-min(ys)
        ox=width*0.025
        kx=(0.95*width)/src.shape[1]
        oy=0.25*height
        ky=(0.5*height)/max(ys)
        print 'Cuillin image scaling ratio {0}/{1} = {2}'.format(kx,ky,kx/ky)
        img=np.ones((height,width))
        r=int(height/25)   
        print 'Line radius is {0} vertices'.format(r)
        r2=r*r
        for x in xrange(src.shape[1]):
            for dy in xrange(-r,r+1):
                py=int(oy+ky*ys[x]+dy)
                for dx in xrange(-r,r+1):
                    px=int(ox+kx*x+dx)
                    d2=dx*dx+dy*dy
                    if d2<r2 and 0<=px and px<width and 0<=py and py<height:
                        img[py,px]=min(img[py,px],1.0-math.sqrt(1.0-float(d2)/float(r2)))
    elif what=='txt_problems':
        img=mktextimage(['Problems are inevitable.','Problems are soluble.'],width,height)
    elif what=='txt_limit':
        img=mktextimage(["What if we didn't take it to our limit...","wouldn't we be forever dissatisfied ?"],width,height)
    elif what=='txt_compass':
        img=mktextimage(['N','E','S','W'],width,height)
    else:
        raise Exception('Unknown decoration option: {0}'.format(what))
        
    imgi=np.uint8(np.rint(np.clip(255.0*img,0.0,255.0)))
    Image.fromarray(imgi).save(filename)
    return img

class BezelMesh(meshtools.mesh.Mesh):

    def __init__(self,density,filename):
        super(BezelMesh,self).__init__()
        self._mkprofile(density,filename)
        self._sweep_profile(density)

    # We work in a y-up space.
    # Same as POV-Ray, but note that yafaray is z-up.
    # Shapeways .obj seems to be z-up too.
    def _mkprofile(self,density,filename):
        z=0.0         # Shorthand for zero, don't change
    
        r=0.5*34.14   # Inner radius
        d=5.5         # Width of bezel.  Not too critical; may overhang watch base if too big.
    
        s=2.4         # "Clip base" inner radius (on 'd' base)
        t=s+1.0       # "Clip base" outer radius
        u=s+1.2       # "Clip lug" outer radius
    
        v=0.65        # Height of bottom of "clip lug"
        w=1.12        # Height of top of "clip lug"
    
        h=w+1.0       # Height of square-section bezel "base"; top arc section sits on top.

        self._decor_base=h

        k=0.5         # Squash factor for arc.

        N=int(d/(2.0*math.pi*(r+0.5*d)/density))  # Match radial resolution to rotational resolution
    
        # TODO: Not so semicircular
        def arc():
            print '{0} radial sections; will use {1} arc points'.format(density,N)
            print 'Arc resolution is approx. {0}mm'.format(d/N)
            p=[i/float(N) for i in xrange(1,N)]
            x=map(lambda v: r+d*v,p)
            y=map(lambda v: h+0.5*k*d*math.sqrt(1.0-sqr(2.0*(v-0.5))),p)
            z=map(lambda v: 0.0,p)
            return zip(x,y,z)
            
        path=[
            (r  , h  ,z)
            ]+arc()+[
            (r+d, h  ,z),
            (r+d, z  ,z),
            (r+t, z  ,z),
            (r+t, v  ,z),
            (r+u, v  ,z),
            (r+u, w  ,z),
            (r+s, w,z),
            (r+s, z  ,z),
            (r  , z  ,z)
            ]
        fig=plt.figure(figsize=(8,8))
        plt.plot(
            [path[i%len(path)][0] for i in range(len(path)+1)],
            [path[i%len(path)][1] for i in range(len(path)+1)]
            )
        plt.axis('equal')
        plt.savefig(filename)

        self._profile=path
        self._displayarea=(density,N)
        self._pixel_index = lambda x,y: x*len(self._profile)+y+1


    def _sweep_profile(self,N):
    
        def rotate(vertices,a):
            ca,sa=math.cos(a),math.sin(a)
            def rot(v):
                return (ca*v[0]+sa*v[2],v[1],-sa*v[0]+ca*v[2])
            return [rot(v) for v in vertices]

        n=len(self._profile)
        self._vertices=list(
            chain.from_iterable(
                (rotate(self._profile,(2.0*math.pi*i)/N) for i in xrange(N))
                )
            )
        self._faces=[
            (
                  i   *n+ j,
                  i   *n+(j+1)%n,
                ((i+1)*n+(j+1)%n)%(N*n),
                ((i+1)*n+ j     )%(N*n),
            ) for j in xrange(n) for i in xrange(N)
            ]

    def decorate(self,what,filename):

        def scale_vertex_y(i,k):
            self.vertices()[i]=(
                self.vertices()[i][0],
                self._decor_base+k*(self.vertices()[i][1]-self._decor_base),
                self.vertices()[i][2]
                )

        # Images bottom edge runs around the inside of the bezel
        img=mkdecorimage(what,self._displayarea[0],self._displayarea[1],filename)
        for y in xrange(img.shape[0]):
            for x in xrange(img.shape[1]):
                scale_vertex_y(self._pixel_index(x,y),img[img.shape[0]-1-y,img.shape[1]-1-x])

def main():
    parser=OptionParser()  # TODO: Upgrade to argparser with Py 2.7
    (options,args) = parser.parse_args()
    if len(args)!=1:
        print 'Provide one positional argument specifying decoration'  # Options: txt_problems,txt_limit,txt_compass,cuillin

    decor=args[0]
    
    density=8*360   # Number of times profile is replicated

    mesh=BezelMesh(density,'bezel-{0}-profile.png'.format(decor))

    mesh.decorate(decor,'bezel-{0}-decor.png'.format(decor))

    meshtools.obj.dump_obj(mesh,'bezel-{0}.obj'.format(decor))
    meshtools.pov.dump_pov(mesh,'bezel-{0}.inc'.format(decor),'bezel-{0}.pov'.format(decor),True,{})

    yafaray_outputs=[
        ('bezel-{0}.xml'.format(decor),0),
        ('bezel-{0}-clay.xml'.format(decor),-1),
        ('bezel-{0}-path.xml'.format(decor),1)
        ]
    meshtools.yafaray.dump_yafaray(mesh,yafaray_outputs,True,{})

if __name__ == "__main__":
    main()
