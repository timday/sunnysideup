Sunnysideup
===========

Software supporting a project to use 3D printing to create a replacement
bezel for a popular, well-known brand of altimeter watch after mine popped
off, and the manufacturer doesn't make the part available as a spare
(an expensive full service seems to be the only option, according to their
own [FAQ](http://www.suunto.com/global/en/support/faqs/Outdoor-Sports-Instruments).

Note that the altimeter-only model is being targetted here.
I've no idea if the results will also be compatible with the
apparently more populat, better-known version which also has
a compass.

Results known to work will be made available through [shapeways](http://www.shapeways.com).
