#!/usr/bin/env python

import Image
import iso
import math
import numpy as np
import numpy.random
import os
import scipy.ndimage.interpolation
import sys

def dot():
    sys.stdout.write('.')
    sys.stdout.flush()

def sqr(x):
    return x*x

# POV-Ray uses y-up
def dump_pov(vertices,triangles,filename_inc,filename_pov):
    
    with open(filename_inc,'w') as f:
        f.write('// File created by "sunnysideup" project\n')
        f.write('// {0} vertices\n'.format(len(vertices)))
        f.write('// {0} triangles\n'.format(len(triangles)))
        f.write('\n')
        f.write('plane {\n')
        f.write(' <0.0,1.0,0.0>,0.0\n')
        f.write(' texture { pigment {rgb <0,0.25,0.0>} finish {ambient 0.0 diffuse 1.0} }\n')
        f.write('}\n')
        f.write('mesh2 {\n')
        f.write('  vertex_vectors {\n')
        f.write('    {0},\n'.format(len(vertices)))
        f.write(',\n'.join(('    <{0},{1},{2}>'.format(v[0],v[1],-v[2]) for v in vertices)))
        f.write('\n  }\n')
        f.write('  face_indices {\n')
        f.write('    {0},\n'.format(len(triangles)))
        f.write(',\n'.join(('    <{0},{1},{2}>'.format(t[0],t[1],t[2]) for t in triangles)))
        f.write('\n  }\n')
        f.write('  texture { pigment {rgb <1,1,1>} finish {ambient 0 diffuse 0.9 specular 0.1} }\n')
        f.write('}\n')

    with open(filename_pov,'w') as f:
        f.write('camera {perspective location <-110,110,0> look_at <0,16,0> angle 22.5}\n')
        f.write('light_source {<-1000,1000,-1000> color rgb<1,1,1>}\n')
        f.write('#include "{0}"\n'.format(filename_inc))

def mkcloud(s,t,m):

    volume=np.zeros((s,s,s))
    k=0.0
    while t<s:
        noise=scipy.ndimage.interpolation.zoom(
            np.random.uniform(-m,m,(t,t,t)),
            float(s)/float(t)
            )
        volume+=noise
        k+=m
        t=2*t
        m=0.5*m

    volume/=k

    c=0.5*(s-1.0)
    def bias(z,y,x):
        return 0.5-np.sqrt(sqr(x-c)+sqr(y-c)+sqr(z-c))/c

    volume+=np.fromfunction(bias,(s,s,s),dtype=np.float32)

    return volume

# PIL 16-bit TIFF loading broken; need to do this
def loadtiff(src_filename):
    src=Image.open(src_filename)
    return np.fromstring(src.tostring(),np.uint16).reshape((src.size[1],src.size[0]))

def main():

    what=2
    if what==0:
        volume=np.zeros((3,3,3))
        volume[1,1,1]=1.0
        vertices,triangles=iso.mkisosurfacemesh(volume,0.5)
        k=24.0
        vertices=[(k*v[0]-k,k*v[1],k*v[2]-k) for v in vertices]
    elif what==1:
        # Random isosurface
        s=64
        c=0.5*(s-1)
        k=30.0/s
        volume=mkcloud(s,8,1.0)
        vertices,triangles=iso.mkisosurfacemesh(volume,0.0)
        vertices=[(k*(c-v[2]),k*v[0],k*(v[1]-c)) for v in vertices]
    elif what==2:
        # Test hedgehog
        R=4
        dir=os.path.expanduser('~/download/digimorph/hedgehog')
        files=[dir+'/'+f for f in sorted(os.listdir(dir)) if len(f)>4 and f[-4:]=='.tif']
        files=[files[i] for i in xrange(0,len(files),R)]
        img0=loadtiff(files[0])
        img0=img0[0::R,0::R]
        size=img0.shape
        print 'Slice size {0}'.format(size)
        volume=np.memmap('volume.tmp', dtype=img0.dtype, mode='w+',shape=(len(files),img0.shape[0],img0.shape[1]))
        print 'Loading (to mmapped numpy file)...'
        for i in xrange(len(files)):
            dot()
            img=loadtiff(files[i])
            img=img[0::R,0::R]
            volume[i,:,:]=img
        print '...loaded'
        
        print 'Range {0} to {1}'.format(np.min(volume),np.max(volume))
        vertices,triangles=iso.mkisosurfacemesh(volume,16384.0)
        cx=0.5*(volume.shape[2]-1)
        cy=0.5*(volume.shape[1]-1)
        cz=0.5*(volume.shape[0]-1)
        k=20.0/cx
        vertices=[(k*(v[2]-cx),k*v[1],k*(v[0]-cz)) for v in vertices]
    else:
        raise Exception('Unknown test')

    dump_pov(vertices,triangles,'test.inc','test.pov')

if __name__ == '__main__':
    main()
