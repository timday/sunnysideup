Volume printing
===============

Attempt to create 3D prints from volume data (not included).
ie Marching cubes to derive surface from volume data, plus
some sort of processing to hollow a model out ("inflate" it 
from the base ?)

Options:
* Implement in python using code [pythonisosurfaces](http://code.google.com/p/pythonisosurfaces/source/browse/modules/p3d.py).  Looks like it generates triangles sharing vertices (minor mod?).  A bit more work for numpy array input ?
* Implement in C++ using [CGAL's mesh generation](http://www.cgal.org/Manual/latest/doc_html/cgal_manual/Mesh_3/Chapter_main.html#Subsection_50.3.1).  Doesn't appear to be extra in CGAL to particularly help with interior.

Initial focus is a dataset obtained courtesy of the 
[Digital Morphology Library](http://www.digimorph.org/index.phtml).

(Data at ~/download/digimorph).

Status
------
* The hedgehog needs crazy memory and produces millions of trianges even before it's 25% of the way through.  
* Therefore tempted to use CGAL for access to its mesh simplification routines.
