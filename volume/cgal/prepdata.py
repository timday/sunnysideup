#!/usr/bin/env python
# -*- coding: utf-8 -*-

import Image
import numpy as np
from optparse import OptionParser
import os
import os.path
import scipy.ndimage.measurements
import scipy.ndimage.morphology
import sys
from tempfile import mkdtemp

sys.path.append(os.path.abspath('../../lib'))
import volumetools.dicom

def dot():
    sys.stdout.write('.')
    sys.stdout.flush()

def pling():
    sys.stdout.write('!')
    sys.stdout.flush()

def sqr(x):
    return x*x

# PIL 16-bit TIFF loading broken; need to do this
def loadtiff(src_filename):
    src=Image.open(src_filename)
    return np.fromstring(src.tostring(),np.uint16).reshape((src.size[1],src.size[0]))

options=None
args=None

def simplify(volume):

    s=scipy.ndimage.morphology.generate_binary_structure(3,3) # Diagonal connectivity

    print 'Thresholding...'
    notair=volume >= options.threshold
    air=volume<options.threshold
    print '...filling...'
    filled=scipy.ndimage.morphology.binary_fill_holes(notair,s)  # Identify what's not reachable from boundaries
    filled=np.logical_and(filled,air)
    print '...editing...'
    return filled*np.uint16(2*options.threshold)+np.logical_not(filled)*volume

def ream(volume,row,col,radius):
    # Hedgehog back end will tolerate a 100 radius hole centred on 450,500 (x,y)
    r2=sqr(radius)
    for y in xrange(-100,101):
        for x in xrange(-100,101):
            if sqr(x)+sqr(y)<=r2:
                volume[volume.shape[0]/4:,row+y,col+x]=0


def edgify(volume):
    s=scipy.ndimage.morphology.generate_binary_structure(3,3)

    print 'Thresholding...'
    notair=volume >= options.threshold
    print '...eroding...'
    notair=scipy.ndimage.morphology.binary_erosion(notair,s,8,border_value=0)
    print '...editing...'
    return notair*np.uint16(0)+np.logical_not(notair)*volume

def salient(volume):
    s=scipy.ndimage.morphology.generate_binary_structure(2,1)  # Or 2,2?
    visible=np.zeros(volume.shape,dtype=np.bool)

    print 'Thresholding...'
    air=volume<options.threshold
    print "{0}%".format((100.0*np.count_nonzero(air))/air.size)

    print '...ray casting...'

    # Trace Z+
    wavefront=np.ones((air.shape[1],air.shape[2]),dtype=np.bool)
    for z in xrange(visible.shape[0]):
        visible[z,:,:]=np.logical_or(visible[z,:,:],wavefront)
        np.logical_and(air[z,:,:],wavefront,wavefront)
#        if z%3==1:
#            wavefront=scipy.ndimage.morphology.binary_dilation(wavefront,structure=s,mask=air[z,:,:],border_value=1)
    print 'Visible: {0}%'.format((100.0*np.count_nonzero(visible))/visible.size)
        
    # Trace Z-
    wavefront=np.ones((air.shape[1],air.shape[2]),dtype=np.bool)
    for z in xrange(visible.shape[0]-1,-1,-1):
        visible[z,:,:]=np.logical_or(visible[z,:,:],wavefront)
        np.logical_and(air[z,:,:],wavefront,wavefront)
#        if z%3==1:
#            wavefront=scipy.ndimage.morphology.binary_dilation(wavefront,structure=s,mask=air[z,:,:],border_value=1)
    print 'Visible: {0}%'.format((100.0*np.count_nonzero(visible))/visible.size)

    # Trace Y+
    wavefront=np.ones((air.shape[0],air.shape[2]),dtype=np.bool)
    for y in xrange(visible.shape[1]):
        visible[:,y,:]=np.logical_or(visible[:,y,:],wavefront)
        np.logical_and(air[:,y,:],wavefront,wavefront)
#        if y%3==1:
#            wavefront=scipy.ndimage.morphology.binary_dilation(wavefront,structure=s,mask=air[:,y,:],border_value=1)
    print 'Visible: {0}%'.format((100.0*np.count_nonzero(visible))/visible.size)

    # Trace Y-
    wavefront=np.ones((air.shape[0],air.shape[2]),dtype=np.bool)
    for y in xrange(visible.shape[1]-1,-1,-1):
        visible[:,y,:]=np.logical_or(visible[:,y,:],wavefront)
        np.logical_and(air[:,y,:],wavefront,wavefront)
#        if y%3==1:
#            wavefront=scipy.ndimage.morphology.binary_dilation(wavefront,structure=s,mask=air[:,y,:],border_value=1)
    print 'Visible: {0}%'.format((100.0*np.count_nonzero(visible))/visible.size)
        
    # Trace X+
    wavefront=np.ones((air.shape[0],air.shape[1]),dtype=np.bool)
    for x in xrange(visible.shape[2]):
        visible[:,:,x]=np.logical_or(visible[:,:,x],wavefront)
        np.logical_and(air[:,:,x],wavefront,wavefront)
#        if x%3==1:
#            wavefront=scipy.ndimage.morphology.binary_dilation(wavefront,structure=s,mask=air[:,:,x],border_value=1)
    print 'Visible: {0}%'.format((100.0*np.count_nonzero(visible))/visible.size)
        
    # Trace X-
    wavefront=np.ones((air.shape[0],air.shape[1]),dtype=np.bool)
    for x in xrange(visible.shape[2]-1,-1,-1):
        visible[:,:,x]=np.logical_or(visible[:,:,x],wavefront)
        np.logical_and(air[:,:,x],wavefront,wavefront)
#        if x%3==1:
#            wavefront=scipy.ndimage.morphology.binary_dilation(wavefront,structure=s,mask=air[:,:,x],border_value=1)
    print 'Visible: {0}%'.format((100.0*np.count_nonzero(visible))/visible.size)

    for i in xrange(8):  # Odd... iterations=16 doesn't work ?
        print '...dilating...'
        visible=scipy.ndimage.morphology.binary_dilation(
            visible,
            structure=scipy.ndimage.morphology.generate_binary_structure(3,3),
            mask=air,
            border_value=1
            )

    print '...editing...'

    solid=volume>=options.threshold
    preserve=np.logical_or(visible,solid)

    print 'Preserved: {0}%'.format(
        (100.0*np.count_nonzero(preserve))/preserve.size
        )

    return preserve*volume+np.logical_not(preserve)*np.uint16(2*options.threshold)

# Fixup the volume for skinning for printing
def cleanup(volume,extvis,edge,crack):

    # volume=simplify(volume)  # Redundant

    if extvis:
        volume=salient(volume)
        volume=simplify(volume)
        volume=np.uint16(volume) # TODO: Why needed ?

    if edge:
        volume=edgify(volume)
        volume=np.uint16(volume) # TODO: Why needed ?

    if crack:
        # Crack it open for test purposes
        volume[:,:,volume.shape[2]/2:]=0

    #ream(volume,500,450,100)  # Good for hedgehog

    return volume

def load_volume_digimorph():

    # The volume is supposedly 0.165mm spacing in z and 78/1024 = .076171875mm in xy
    # Or dimensionless z = 0.165 / (78/1024) = ~ 2.166

    dir=os.path.expanduser('~/download/volume_data/digimorph/hedgehog')
    files=[dir+'/'+f for f in sorted(os.listdir(dir)) if len(f)>4 and f[-4:]=='.tif']
    img0=loadtiff(files[0])
    size=img0.shape

    volume=np.memmap('volume.tmp',shape=(len(files),img0.shape[0],img0.shape[1]),dtype=np.uint16,mode='w+')
    
    print 'Loading...'
    for i in xrange(len(files)):
        dot()
        img=loadtiff(files[i])
        volume[i,:,:]=img
    print '...loaded {0}'.format(volume.shape)

    return volume,(1.0,1.0,2.166)

def zeropadvolume(src):
    dst=np.zeros((src.shape[0]+4,src.shape[1]+4,src.shape[2]+4),dtype=src.dtype)
    dst[2:-2,2:-2,2:-2]=src
    return dst

def trash(dir):
    try:
        print 'Trashing {0}...'.format(dir)
        for root, dirs, files in os.walk(dir, topdown=False):
            for name in files:
                os.remove(os.path.join(root, name))
            for name in dirs:
                print 'Removing {0} in {1}'.format(name,root)
                os.rmdir(os.path.join(root, name))
        os.rmdir(dir)
    except OSError:
        print 'Problem in trash'
    print '...trashed {0}'.format(dir)

def main():

    parser=OptionParser()  # TODO: Upgrade to argparser with Py 2.7

    # Good threshold values are 850 for PHENIX, 4096+2048 for hedgehog
    parser.add_option('-t','--threshold',dest='threshold',help='Iso-surface threshold',type='float',metavar='T',default='1024.0')
    parser.add_option('-p','--process',dest='process',help='Processing options (bit select 1: extvis, 2: edge, 4:crack)',type='int',metavar='P',default=0)
    parser.add_option('-v','--volume',dest='volume',help='Volume',type='string',metavar='VOL')

    global options
    global args
    (options,args) = parser.parse_args()
    
    if options.volume == 'digimorph':
        volume,voxel_size=load_volume_digimorph()
    elif options.volume == 'phenix':
        volume,voxel_size=volumetools.dicom.load_volume_phenix()
    elif options.volume == 'manix':
        volume,voxel_size=volumetools.dicom.load_volume_manix()
    else:
        raise Exception('No recognised volume option')

    # Avoid problems at end ?
    volume=zeropadvolume(volume)
    
    opt_extvis=((options.process&1)!=0)
    opt_edge=((options.process&2)!=0)
    opt_crack=((options.process&4)!=0)
    volume=cleanup(volume,opt_extvis,opt_edge,opt_crack)

    print 'Writing slices...'
    trash('slices')
    os.mkdir('slices')
    for z in xrange(volume.shape[0]):
        filename='slices/{0:04d}.png'.format(z)
        slice=np.uint8(np.rint(np.clip(volume[z]*(255.0/(2.0*options.threshold)),0.0,255.0)))
        Image.fromarray(slice).save(filename)

    print 'Writing to monolithic file...'
    out=file('volume.inr','wb')

    header="""#INRIMAGE-4#{{
XDIM={0}
YDIM={1}
ZDIM={2}
VDIM=1
TYPE=unsigned fixed
PIXSIZE=16 bits
CPU=pc
VX={3}
VY={4}
VZ={5}
""".format(
        volume.shape[2],volume.shape[1],volume.shape[0],
        voxel_size[0],voxel_size[1],voxel_size[2]
        )

    while len(header)<256-4:
        header+='\n'
    header+='##}\n'

    assert len(header)==256

    out.write(header)
    volume.tofile(out)

    print '...wrote to file'

if __name__ == '__main__':
    main()
