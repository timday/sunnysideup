#!/usr/bin/env python

import math
from optparse import OptionParser
import os
import sys

sys.path.append(os.path.abspath('../../lib'))
import meshtools.mesh
import meshtools.obj
import meshtools.pov
import meshtools.yafaray

def cross(a,b):
    return (
        a[1]*b[2]-a[2]*b[1],
        a[2]*b[0]-a[0]*b[2],
        a[0]*b[1]-a[1]*b[0]
        )

def sqr(x):
    return x*x

def sub(a,b):
    return (a[0]-b[0],a[1]-b[1],a[2]-b[2])

def norm(a):
    m=math.sqrt(sqr(a[0])+sqr(a[1])+sqr(a[2]))
    return (a[0]/m,a[1]/m,a[2]/m)

def up(src,dst):
    front=sub(dst,src)
    right=cross(front,(0.0,0.0,1.0))
    up=cross(right,front)
    return norm(up)

def main():
    
    parser=OptionParser()  # TODO: Upgrade to argparser with Py 2.7
    parser.add_option('-r','--rotate',dest='rotation',help='Spin camera by angle A',type='float',metavar='A',default=0.0)
    parser.add_option('-u','--upsidedown',dest='upsidedown',help='Spin upsidedown',action='store_true',default=False)
    parser.add_option('-X','--clip-x',dest='clip_x',help='Clip x<0',action='store_true',default=False)
    parser.add_option('-Y','--clip-y',dest='clip_y',help='Clip y>0',action='store_true',default=False)
    parser.add_option('-Z','--clip-z',dest='clip_z',help='Clip z<0',action='store_true',default=False)
    (options,args) = parser.parse_args()

    print 'Rotation angle {0}'.format(options.rotation)

    lookat=(0.0,0.0,0.0)

    pos_a=options.rotation*math.pi/180.0
    pos_ca=math.cos(pos_a)
    pos_sa=math.sin(pos_a)
    pos=(-128.0*pos_ca,-128.0*pos_sa,32.0)

    light_a=pos_a+math.pi/6.0
    light_ca=math.cos(light_a)
    light_sa=math.sin(light_a)
    light=(-500.0*light_ca,-500.0*light_sa,500.0)
    
    parameters={
        'camera': pos,
        'camera_up': up(pos,lookat),
        'light': light,
        'canvas_width': 960,
        'canvas_height': 960
        }

    print 'Loading...'
    mesh=meshtools.obj.load_obj('out.obj')
    print '...loaded'

    if options.upsidedown:
        print "Flipping upside-down"
        mesh=mesh.upsidedown()

    if options.clip_x:
        mesh=mesh.clipped((-1.0,0.0,0.0),0.0)
    if options.clip_y:
        mesh=mesh.clipped((0.0,1.0,0.0),0.0)
    if options.clip_z:
        mesh=mesh.clipped((0.0,0.0,1.0),0.0)

    print 'Dumping POV...'
    meshtools.pov.dump_pov(mesh,'out.inc','out.pov',True,parameters)
    print '...dumped POV, dumping yafaray...'
    yafaray_outputs=[
        ('out.xml',0),
        ('out-clay.xml',-1)
#        ('out-path.xml',1)
        ]
    meshtools.yafaray.dump_yafaray(mesh,yafaray_outputs,True,parameters)
    print '...dumped yafaray'

if __name__ == "__main__":
    main()
