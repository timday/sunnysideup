#include <CGAL/Aff_transformation_3.h>
#include <CGAL/Cartesian.h>
#include <CGAL/Surface_mesh_default_triangulation_3.h>
#include <CGAL/Surface_mesh_default_criteria_3.h>
#include <CGAL/Complex_2_in_triangulation_3.h>
#include <CGAL/IO/Complex_2_in_triangulation_3_file_writer.h>
#include <CGAL/IO/File_writer_wavefront.h>
#include <CGAL/IO/Geomview_stream.h>
#include <CGAL/IO/output_surface_facets_to_polyhedron.h>
#include <CGAL/IO/Polyhedron_geomview_ostream.h>
#include <CGAL/IO/print_wavefront.h>
#include <CGAL/make_surface_mesh.h>
#include <CGAL/Gray_level_image_3.h>
#include <CGAL/Implicit_surface_3.h>
#include <CGAL/Polyhedron_3.h>
#include <CGAL/Vector_3.h>

#include <algorithm>
#include <boost/array.hpp>
#include <boost/filesystem.hpp>
#include <boost/format.hpp>
#include <boost/program_options.hpp>
#include <boost/gil/gil_all.hpp>
#include <boost/gil/extension/io/png_io.hpp> // Must come after gil_all.h
#include <fstream>
#include <iostream>
#include <map>
#include <string>
#include <vector>

template <typename T> T sqr(T x) {return x*x;}

// See
//   http://www.cgal.org/Manual/latest/doc_html/cgal_manual/Surface_mesher/Chapter_main.html
// and
//   http://www.cgal.org/Manual/latest/doc_html/cgal_manual/Surface_mesher_ref/Class_Gray_level_image_3.html
// INRIMAGE format described at http://serdis.dis.ulpgc.es/~krissian/InrView1/IOformat.html
// GIS and ANALYSE at http://brainvisa.info/forum/viewtopic.php?t=167 

// default triangulation for Surface_mesher
typedef CGAL::Surface_mesh_default_triangulation_3 Tr;

// c2t3
typedef CGAL::Complex_2_in_triangulation_3<Tr> C2t3;

typedef Tr::Geom_traits GT;
typedef CGAL::Gray_level_image_3<GT::FT, GT::Point_3> Gray_level_image;
typedef CGAL::Implicit_surface_3<GT, Gray_level_image> Surface_3;

typedef CGAL::Polyhedron_3<GT>         Polyhedron;
typedef CGAL::Aff_transformation_3<GT>  Transformation;

void write_c2t3_to_obj(const C2t3& c2t3,const Transformation& transform,const char* filename)
{
  std::ofstream out(filename,std::ofstream::out);

  std::map<Tr::Vertex_handle,size_t> vertex_indices;
  std::vector<GT::Point_3> vertices;

  const Tr& tr=c2t3.triangulation();
  for (
    Tr::Finite_vertices_iterator it=tr.finite_vertices_begin();
    it!=tr.finite_vertices_end();
    ++it
  ) {
    vertex_indices.insert(std::make_pair(it,vertices.size()));
    vertices.push_back(it->point());
  }
  std::transform(vertices.begin(),vertices.end(),vertices.begin(),transform);

  std::vector<boost::array<size_t,3> > triangles;
  for (
    Tr::Finite_facets_iterator it=tr.finite_facets_begin();
    it!=tr.finite_facets_end();
    ++it
  ) {
    const typename Tr::Cell_handle cell=it->first;
    const int& index=it->second;
    if (cell->is_facet_on_surface(index)==true)
    {
      boost::array<size_t,3> triangle;
      triangle[0]=vertex_indices[cell->vertex(tr.vertex_triple_index(index,0))];
      triangle[1]=vertex_indices[cell->vertex(tr.vertex_triple_index(index,1))];
      triangle[2]=vertex_indices[cell->vertex(tr.vertex_triple_index(index,2))];
      triangles.push_back(triangle);
    }
  }

  out << "# " << vertices.size() << " vertices" << std::endl;
  out << "# " << triangles.size() << " triangles" << std::endl;
  out << std::endl;

  for (size_t i=0;i<vertices.size();++i)
    out << "v " << vertices[i].x() << " " << vertices[i].y() << " " << vertices[i].z() << std::endl;

  out << std::endl;

  for (size_t i=0;i<triangles.size();++i)
    out << "f " << triangles[i][0]+1 << " " << triangles[i][1]+1 << " " << triangles[i][2]+1 << std::endl;
  
  out.close();
}

int main(int argc,char** argv) {

  boost::program_options::options_description desc("Options");
  desc.add_options()
    ("help,h","show help message")
    ("seed,s",boost::program_options::value<std::vector<float> >()->multitoken())
    ("ballsize,b",boost::program_options::value<float>()->default_value(4.0f))
    ("threshold,t",boost::program_options::value<float>()->default_value(1024.0f));
  boost::program_options::variables_map vm;
  boost::program_options::store(boost::program_options::parse_command_line(argc,argv,desc),vm);
  boost::program_options::notify(vm);

  const float threshold=vm["threshold"].as<float>();
  std::cout << "Threshold is " << threshold << std::endl;

  const float ballsize=vm["ballsize"].as<float>();
  std::cout << "Ball size is " << ballsize << std::endl;

  const std::vector<float> seed=vm["seed"].as<std::vector<float> >();
  if (seed.size()!=3)
    throw std::runtime_error("Need 3 arguments to seed option");
  std::cout << "Seed point is " << seed[0] << "," << seed[1] << "," << seed[2] << std::endl;
  
  Tr tr;            // 3D-Delaunay triangulation
  C2t3 c2t3 (tr);   // 2D-complex in 3D-Delaunay triangulation

  // the 'function' is a 3D gray level image
  //Gray_level_image image("/home/local/tmp/examples/Surface_mesher/data/skull_2.9.inr", 2.9f);

  // Hedgehog is 784, 1024, 1024 (with 1x1x1 voxel size for now)
  // 8192.0f good, 1024.0 & 2048.0 a solid block, 4096.0 just starting to appear, 4096.0f+1024.0f too much memory!

  std::cout << "Loading volume..." << std::endl;
  Gray_level_image image("volume.inr",threshold);  
  std::cout << "...loaded volume" << std::endl;

  // Carefully choosen bounding sphere: the center must be inside the
  // surface defined by 'image' and the radius must be high enough so that
  // the sphere actually bounds the whole image.

  // Don't use actual voxel size in mm because doesn't seem to extract such nice surfaces
  //const double VXmm=0.076171875;
  //const double VYmm=0.076171875;
  //const double VZmm=0.165;

  const double VX=image.vx();
  const double VY=image.vy();
  const double VZ=image.vz();

  const unsigned int SX=image.xdim();
  const unsigned int SY=image.ydim();
  const unsigned int SZ=image.zdim();

  const double CX=SX*0.5*VX;
  const double CY=SY*0.5*VY;
  const double CZ=SZ*0.5*VZ;

  /*
  boost::optional<std::pair<double,double> > c;
  for (double y=0.5*CY;y<1.5*CY && !c;y+=1.0) {
    for (double x=0.5*CX;x<1.5*CX && !c;x+=1.0) {
      const GT::FT v=image(GT::Point_3(x,y,CZ));
      if (v<0) {
	c=std::make_pair(x,y);
      }
    }    
  }
  if (!c) throw std::runtime_error("Couldn't find internal point in central slice");
  const double cx=c.get().first;
  const double cy=c.get().second;
  */

  const double seed_x=seed[0]*VX;
  const double seed_y=seed[1]*VY;
  const double seed_z=seed[2]*VZ;

  GT::Point_3 bounding_sphere_center(seed_x,seed_y,seed_z);
  GT::FT bounding_sphere_squared_radius = 1.0625*(sqr(SX*VX)+sqr(SY*VY)+sqr(CZ*VZ));  // Was using 2048 for hedgehog
  GT::Sphere_3 bounding_sphere(bounding_sphere_center,bounding_sphere_squared_radius);

  const GT::FT c=image(GT::Point_3(seed_x,seed_y,seed_z));
  const GT::FT v=::triLinInterp(image.image(),seed_x,seed_y,seed_z,0.0);

  // TODO: Something strange going on here. Scan and see how many voxels are interior/exterior.

  boost::system::error_code err;
  boost::filesystem::remove_all("slices-interp",err);
  boost::filesystem::create_directory("slices-interp");
  
  for (unsigned int z=0;z<SZ;++z) {
    boost::gil::gray8_image_t dstimg(SX,SY);
    boost::gil::gray8_view_t dst=boost::gil::view(dstimg);
    for (int y=0;y<dst.height();++y)
      for (int x=0;x<dst.width();++x) {
	const GT::FT v=::triLinInterp(image.image(),x*VX,y*VY,z*VZ);
	const float p=std::max(0.0f,std::min(floorf(0.5+255.0f*v/(2.0f*threshold)),255.0f));
	dst(x,y)=static_cast<unsigned char>(p);
      }
    const std::string filename=boost::str(boost::format("slices-interp/%04d.png") % z);
    boost::gil::png_write_view(filename,dst);
  }

  std::ostringstream msg;
  msg << "Seed point: got value " << c << ", interpolated " << v;
  if (c != GT::FT(-1)) {
    throw std::runtime_error("Not an interior seed point (want value -1)");
  }

  GT::Vector_3 shift(-CX,-CY,-CZ);
  Transformation translate(CGAL::TRANSLATION, shift);
  Transformation scale(CGAL::SCALING,25,SX);

  // This works good for the thing standing up
  Transformation orient
    (
     0.0,-1.0, 0.0,  0.0,  // Flip xy gets it facing
     -1.0, 0.0, 0.0,  0.0,
     0.0, 0.0,-1.0,  0.0   // -z Flips the head to top
     );
  
  /*
  // Alternative for fit to image
  // OK but lying on it's side looking dead.  Messy side up.
  Transformation orient
    (
     0.0,-1.0, 0.0,  0.0, 
     0.0, 0.0,-1.0,  0.0,
     1.0, 0.0, 0.0,  0.0  
     );
    
  const boost::array<boost::array<double,3>,3> basis={{
      {1.0,0.0,0.0},
      {0.0,1.0,0.0},
      {0.0,0.0,1.0}
    }};
  
    // Explanation:
    // 0: XYZ: Standing on nose, sideways
    // 1: XZY: Lying on back.
    // 2: YXZ: On nose, back to camera.
    // 3: YZX: On back, head to camera.
    // 4: ZXY: On side, back to camera.
    // 5: ZYX: On side, nose to camera.
    // Conclusions: 1 promising if other way up
    for (int bx=0;bx<3;++bx) {
      for (int by=0;by<3;++by) {
	if (by!=bx) {
	  for (int bz=0;bz<3;++bz) {
	    if (bz!=bx && bz!=by) {
	      Transformation orient
		(
 		 basis[bx][0],basis[by][0],basis[bz][0], 0.0,
 		 basis[bx][1],basis[by][1],basis[bz][1], 0.0,
 		 basis[bx][2],basis[by][2],basis[bz][2], 0.0
		 );
	      //	      orientations.push_back(orient);
	    }
	  }
	}
      }
    }
  */
  // Based on XZY but invert.
  // Works good for a lying down one, if camera position got some attention
//  Transformation orient
//    (
//     1.0, 0.0, 0.0,  0.0, 
//     0.0, 0.0, 1.0,  0.0,
//     0.0,-1.0, 0.0,  0.0  
//     );
    
  // definition of the surface, with relative precision (whatever that means)
  Surface_3 surface(image, bounding_sphere, 1e-5);

  // defining meshing criteria: angular lower bound, "surface ball" upper bound, some distance upper bound

  // Ballsize 5.0 just works for digimorph
  CGAL::Surface_mesh_default_criteria_3<Tr> criteria(10.,ballsize,ballsize);

  // meshing surface, with the "manifold without boundary" algorithm
  std::cout << "Meshing..." << std::endl;  
  CGAL::make_surface_mesh(c2t3, surface, criteria, CGAL::Non_manifold_tag(),360);  // Fails to create polyhedron when Non_manifold_tag is set
  std::cout << "...meshed " << c2t3.number_of_facets() << " facets" << std::endl;  

  const Transformation transform = orient*scale*translate;

  write_c2t3_to_obj(c2t3,transform,"out.obj");

  /*
  {
    std::ofstream out("out.off");
    CGAL::output_surface_facets_to_off (out, c2t3);
    std::cout << "Final number of points: " << tr.number_of_vertices() << "\n";
  }
  */

  /*
  // Code to dump polyhedron, but this doesn't work for non-manifold stuff
  std::cout << "Creating polyhedron..." << std::endl;
  Polyhedron poly;
  const bool poly_result=CGAL::output_surface_facets_to_polyhedron(c2t3,poly);
  std::cout << "...created polyhedron... (" << (poly_result ? "true" : "false" ) << ")" << std::endl;
  std::transform( poly.points_begin(), poly.points_end(), poly.points_begin(), transform);
  std::cout << "...transformed polyhedron..." << std::endl;
    
  std::ofstream out("out.obj");
  CGAL::print_polyhedron_wavefront(out,poly);
  
  std::cout << "...wrote output" << std::endl;
  */

  /*
  CGAL::Geomview_stream gv(CGAL::Bbox_3(-100, -100, -100, 100, 100, 100));
  gv.clear();
  gv.set_bg_color(CGAL::Color(0, 0, 64));
  gv.set_wired(true);
  gv.set_line_width(4);
  gv << poly;

  std::cout << "Ctrl-D finish" << std::endl;
  char ch;
  std::cin >> ch;
  */

  return 0;
}
