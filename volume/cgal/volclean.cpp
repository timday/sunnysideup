#include <boost/bind.hpp>
#include <boost/dynamic_bitset.hpp>
#include <boost/function.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/optional.hpp>
#include <fcntl.h>
#include <fstream>
#include <iostream>
#include <memory>
#include <sstream>
#include <stdexcept>
#include <string>
#include <sys/mman.h>

template <typename T> void chkset(boost::optional<T>& dst,const std::string& src) {
  if (dst) {
    throw std::runtime_error("Attempt to set already set value");
  } else {
    dst=boost::lexical_cast<T>(src);
  }
}

typedef unsigned int uint;
typedef unsigned short ushort;

class domain_dense;
class domain_sparse;

// Monolithic volume
class volume
{
public:

  volume(size_t sx,size_t sy,size_t sz,ushort* data,const boost::function<void()>& cleanup)
    :_sx(sx)
    ,_sy(sy)
    ,_sz(sz)
    ,_n(sx*sy*sz)
    ,_data(data)
    ,_cleanup(cleanup)
  {}
   
  ~volume()
  {
    _cleanup();
  }

  ushort get_voxel(size_t i) const {
    return _data[i];
  }

  std::unique_ptr<domain_dense> below(ushort v) const;

private:
  const size_t _sx;
  const size_t _sy;
  const size_t _sz;
  const size_t _n;
  ushort*const _data;
  const boost::function<void()> _cleanup;
};

class domain_dense
{
public:
  domain_dense(size_t sx,size_t sy,size_t sz)
    :_sx(sx)
    ,_sy(sy)
    ,_sz(sz)
    ,_n(sx*sy*sz)
    ,_data(_n)
  {}
  ~domain_dense()
  {}

  void set_voxel(size_t i,bool v) {
    _data.set(i,v);
  }
    
private:
  const size_t _sx;
  const size_t _sy;
  const size_t _sz;
  const size_t _n;
  boost::dynamic_bitset<unsigned long long int> _data;
};

void cleanup(void* ptr,size_t n,int fd) {
  if (munmap(ptr,n)!=0)
    std::cerr << "cleanup: unmap failed" << std::endl;
  if (close(fd)!=0)
    std::cerr << "cleanup: close failed" << std::endl;    
}

std::unique_ptr<domain_dense> volume::below(ushort v) const
{
  std::unique_ptr<domain_dense> dst(new domain_dense(_sx,_sy,_sz));
  for (size_t i=0;i<_n;++i)
    dst->set_voxel(i,get_voxel(i)<v);
  return dst;
}

std::unique_ptr<volume> mmap_inr_file_to_volume(const std::string& filename) {
  std::ifstream file(filename);
  std::vector<char> buffer(257,'\0');
  file.read(&buffer[0],256);
  if (!file)
    throw std::runtime_error("Error opening file");
  file.close();
  const std::string header(&buffer[0]);
  if (header.size()!=256 || header[255]!='\n' || header[254]!='}' || header[253]!='#' || header[252]!='#') {
    throw std::runtime_error("Bad header in file");
  }

  boost::optional<uint> sx;
  boost::optional<uint> sy;
  boost::optional<uint> sz;
  boost::optional<float> vx;
  boost::optional<float> vy;
  boost::optional<float> vz;
  
  std::istringstream in(header);
  std::string line;
  bool initial=true;
  while (std::getline(in,line)) {
    if (initial) {
      if (line!="#INRIMAGE-4#{") {
	throw "No recognized initial INRIMAGE line found";
      }
      initial=false;
    } else if (line=="##}") {
      break;
    } else if (line=="") {
      continue;
    } else {
      const size_t eq_pos=line.find('=');
      if (eq_pos==std::string::npos)
	throw std::runtime_error("Unexpected '='-less line in header: "+line);
      const std::string key=line.substr(0,eq_pos);
      const std::string value=line.substr(eq_pos+1);
      
      if (key=="XDIM") 
	chkset(sx,value);
      else if (key=="YDIM")
	chkset(sy,value);
      else if (key=="ZDIM")
	chkset(sz,value);
      else if (key=="VX")
	chkset(vx,value);
      else if (key=="VY")
	chkset(vy,value);
      else if (key=="VZ")
	chkset(vz,value);
      else if (key=="VDIM") {
	if (value!="1")
	  throw std::runtime_error("Unexpected value for VDIM");
      } else if (key=="PIXSIZE") {
	if (value!="16 bits")
	  throw std::runtime_error("Unexpected value for TYPE");
      } else if (key=="TYPE") {
	if (value!="unsigned fixed")
	  throw std::runtime_error("Unexpected value for TYPE");
      } else if (key=="CPU") {
	if (value!="pc")
	  throw std::runtime_error("Unexpected value for CPU");
      } else {
	throw std::runtime_error("Unexpected key");
      }
    }
  }

  if (!sx || !sy || !sz) {
    throw std::runtime_error("One or more volume dimensions unknown");
  }

  int fd=open(filename.c_str(),O_RDWR);
  if (fd==-1) {
    throw std::runtime_error("Failed to open file");
  }
  const size_t n=sx.get()*sy.get()*sz.get()*sizeof(ushort);
  void*const mapped=mmap(0,n,PROT_READ|PROT_WRITE,MAP_SHARED|MAP_POPULATE,fd,0);
  if (mapped==MAP_FAILED) {
    throw std::runtime_error("Failed to mmap file");
  }

  const boost::function<void()> cleanupfn=boost::bind(cleanup,mapped,n,fd);
  char*const bytes=reinterpret_cast<char*>(mapped);
    
  return std::unique_ptr<volume>(
    new volume(sx.get(),sy.get(),sz.get(),reinterpret_cast<ushort*>(bytes+256),cleanupfn)
  );
}

int main(int,char**) {
  
  std::unique_ptr<volume> vol=mmap_inr_file_to_volume("hedgehog.inr");
  std::cout << "Mapped" << std::endl;
  std::unique_ptr<domain_dense> air=vol->below(4096+2048);
  std::cout << "Thresholded" << std::endl;
  return 0;
}

// Plan:
// Identify all all air voxels reachable from the perimeter.
// Fill all unreachable air as irrelevant.
// Erode all the reachable air voxels.
// Classify the remaining as exterior connected or not.
// Dilate each class into air again until all reached.
// Whichever one gets there first wins.
// ...but this will only fail to intrude all the way into cavities which leave some stuff behind.
