#!/usr/bin/env python

import Image
import numpy as np
import os
import os.path
import scipy.ndimage.measurements
import scipy.ndimage.morphology
import sys
from tempfile import mkdtemp

def dot():
    sys.stdout.write('.')
    sys.stdout.flush()

def pling():
    sys.stdout.write('!')
    sys.stdout.flush()

def sqr(x):
    return x*x

# PIL 16-bit TIFF loading broken; need to do this
def loadtiff(src_filename):
    src=Image.open(src_filename)
    return np.fromstring(src.tostring(),np.uint16).reshape((src.size[1],src.size[0]))

threshold=4096.0+2048.0    # As per mkmsh.cpp

# Simply fill any unreachable air holes
def simplify(volume):

    s=scipy.ndimage.morphology.generate_binary_structure(3,3) # Diagonal connectivity

    print 'Thresholding...'
    notair=volume >= threshold
    print '...filling...'
    notair=scipy.ndimage.morphology.binary_fill_holes(notair,s)  # Identify what's not reachable from boundaries
    print '...editing...'
    return notair*np.uint16(8192)+np.logical_not(notair)*volume

# Create voids separated from existing airspaces
def edgify(volume):
    s=scipy.ndimage.morphology.generate_binary_structure(3,1) # Adjacent connectivity

    print 'Thresholding...'
    notair=volume >= threshold
    print '...eroding...'
    notair=scipy.ndimage.morphology.binary_erosion(notair,s,16,border_value=0)
    print '...editing...'
    return notair*np.uint16(0)+np.logical_not(notair)*volume

def external_air(volume):
    s0=scipy.ndimage.morphology.generate_binary_structure(3,1)
    s1=scipy.ndimage.morphology.generate_binary_structure(3,3)
    print 'Thresholding...'
    air=volume < threshold
    print '...eroding...'
    air=scipy.ndimage.morphology.binary_erosion(air,structure=s0,iterations=4,border_value=1)
    print '...allocating...'
    labeled=np.zeros(volume.shape,dtype=np.int32)
    print '...labeling...'
    num_objects=scipy.ndimage.measurements.label(air,s1,output=labeled)
    external_air_label=labeled[0,0,0]
    print '...labeled {0} objects, exterior air is {1}...'.format(num_objects,external_air_label)
    air = labeled==external_air_label
    labeled=None
    allow=volume<threshold
    for i in xrange(16):
        dot()
        air = scipy.ndimage.morphology.binary_dilation(air,structure=s1,mask=allow,border_value=1)
    return air

def fill_cavities(volume):
    preserve=external_air(volume)
    print '...combining...'
    preserve=np.logical_or(preserve,volume>=threshold)
    print '...editing...'
    return preserve*volume+np.logical_not(preserve)*np.uint16(8192)
    
def ream(volume):
    # Hedgehog back end will tolerate a 100 radius hole centred on 450,500 (x,y)
    for y in xrange(-100,101):
        for x in xrange(-100,101):
            if sqr(x)+sqr(y)<=sqr(100):
                volume[volume.shape[0]/4:,500+y,450+x]=0

# Fixup the volume for skinning for printing
def cleanup(volume):

    # Fill any interior airspaces
    volume=simplify(volume)  

    # Fill any internal but externally connected cavities here
    volume=fill_cavities(volume)

    # Hollow out the solid structure
    volume=edgify(volume)

    # Make drainable
    print '...reaming...'
    ream(volume)

    # One last pass to close up any sealed-off zones
    volume=simplify(volume) 

    volume[:,:,512:]=0   # Inspect surface
    return volume

def load_volume():
    dir=os.path.expanduser('~/download/digimorph/hedgehog')
    files=[dir+'/'+f for f in sorted(os.listdir(dir)) if len(f)>4 and f[-4:]=='.tif']
    img0=loadtiff(files[0])
    size=img0.shape

    volume=np.memmap('volume.tmp',shape=(len(files),img0.shape[0],img0.shape[1]),dtype=np.uint16,mode='w+')
    
    print 'Loading...'
    for i in xrange(len(files)):
        dot()
        img=loadtiff(files[i])
        volume[i,:,:]=img
    print '...loaded {0}'.format(volume.shape)

    return volume

def main():
    
    volume=load_volume()
    volume=cleanup(volume)

    print 'Writing to file...'
    out=file('hedgehog.inr','wb')
    # The volume is supposedly 0.165mm spacing in z and 78/1024 = .076171875mm in xy
    # Or dimensionless z = 0.165 / (78/1024) = ~ 2.166

    header="""#INRIMAGE-4#{{
XDIM={0}
YDIM={1}
ZDIM={2}
VDIM=1
TYPE=unsigned fixed
PIXSIZE=16 bits
CPU=pc
VX=1.0
VY=1.0
VZ=2.166
""".format(volume.shape[2],volume.shape[1],volume.shape[0])

    while len(header)<256-4:
        header+='\n'
    header+='##}\n'

    assert len(header)==256

    out.write(header)
    volume.tofile(out)

    print '...wrote to file'

if __name__ == '__main__':
    main()
