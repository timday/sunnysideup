
Attempt to use CGAL for mesh generation.

On Debian, installed

  libcgal-dev libcgal-demo libcgal-ipelets

Also

  geomview

but not particularly useful for huge poly counts.

-----

Morphology based approach isn't working.
Very hard to distinguish interior ducts and voids from genuine surface detail.
Need some measure of depth/saliency.

* Set up diffusion equation as sparse matrix.
Iterate forward with volume boundary as source, surfaces as sink.
Will tend to steady state.
NO.  Need too many entries; all air cells are represented, not just surface.

* Set up big 3D diffusion equation ?  ~4GB to 4GB iterations.
Tends to steady state; just threshold and fill?
MAYBE.  Might take 1000s of iterations.

* Ray cast from all surface points in all directions.
Or some approximations.
Mark all air cells hit.
Dilate hits to fudge a little.
Make invisible remainder solid.
Skin.


